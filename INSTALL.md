# Install Knative

To install Knative on a local cluster, use:

```bash
kubectl apply -f https://github.com/knative/serving/releases/download/v0.26.0/serving-crds.yaml
kubectl apply -f https://github.com/knative/serving/releases/download/v0.26.0/serving-core.yaml
kubectl apply -f https://github.com/knative/net-kourier/releases/download/v0.26.0/kourier.yaml

kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress.class":"kourier.ingress.networking.knative.dev"}}'

kubectl patch configmap/config-domain \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"knative.minikube":""}}'

kubectl patch configmap/config-deployment \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"registriesSkippingTagResolving":"kind.local,ko.local,dev.local,localhost:5000"}}'
```

## Access to cluster

To get access to the ingress controller use:

```bash
minikube tunnel
```

## Configure registry

To configure the minikube registry use:

```bash
minikube addons enable registry
```

## Create DNS alias

To create a DNS alias use Dnsmasq:

```bash
brew install dnsmasq
```

Configure .knative.minikube to the Cluster IP of the kourier (retrieved with `kubectl get svc -n kourier-system`) in /usr/local/etc/dnsmasq.conf:

```
address=/.knative.minikube/10.103.177.46
```

Then start dnsmasq with:

```bash
sudo brew services start dnsmasq
```

Add .minikube to resolvers with:

```bash
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/minikube'
```

Finally run the tunnel with:

```bash
minikube tunnel
```