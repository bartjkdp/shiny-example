# Shiny example

An example how to run a Shiny dashboard with [Docker](https://docker.com/).

## Prerequisites

Make sure you installed [Docker](https://docs.docker.com/get-docker/) on your machine.

## Build the image

Build the image with:

```bash
docker build -t shiny-example .
```

## Run the image

Run the image on your local machine with:

```bash
docker run -p 3838:3838 shiny-example
```

View the dashboard in your browser on http://localhost:3838.

## Publish via Knative

Make the Docker image available on minikube with:

```bash
eval $(minikube -p minikube docker-env)
docker build -t localhost:5000/shiny-example .
docker push localhost:5000/shiny-example
```

```bash
kn service create shiny-example \
--image localhost:5000/shiny-example \
--port 3838 \
--revision-name=shiny-example
```
